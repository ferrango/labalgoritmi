#include <fstream>
//#include <iostream>

using namespace std;


int main()
{
  int nElementi;
  int sommaAtt;
  int maxPast;
  int bb;
  maxPast = 0;
  sommaAtt = 0;

  ifstream in("input.txt");
  in>>nElementi;
       
  for(int i=0;i<nElementi;i++)
  {    
    in >> bb;
    sommaAtt = max(sommaAtt + bb, 0);
    maxPast = max(sommaAtt, maxPast);
  }
  
  in.close();
   
  ofstream oo;
  oo.open("output.txt");
  oo << maxPast;
  
  oo.close();
    
  return 0;
}
