#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

bool * nodiVisti;
int * percorsi;
int quanti;
int percorsoWin;

vector<int> * vettoreGrafo;
int * daQuiCiMetto;

void Explora(int nodo)
{
	nodiVisti[nodo] = true;
	for (int i = 0; i < vettoreGrafo[nodo].size(); i++)
	{
		// cout << "sono al sottonodo " << i << " di " << vettoreGrafo[nodo].size() << endl;
		if(nodiVisti[vettoreGrafo[nodo][i]] == false)
			Explora(vettoreGrafo[nodo][i]);
	}
}

void StampaGrafo(int NumNodi)
{
	for(int i = 0; i < NumNodi; i++)
	{
		cout << "Ha " << vettoreGrafo[i].size() << " figli\n";
		cout << "Nodo " << i << " -> ";
		for(int j = 0; j < vettoreGrafo[i].size(); j++)
			cout << vettoreGrafo[i][j] << " ";
		cout << endl;
	} 
}

void ResettaEsplorati(int start, int end)
{
	for(int i = start; i < end; i++)
		nodiVisti[i] = false;
}

bool SoiRiva(int dest, int nodo, int passi)
{
	nodiVisti[nodo] = true;
	bool trovato = false;
	
	if(nodo == dest)
	{
		if(passi < percorsoWin)
		{
		    percorsoWin = passi;
			quanti = 1;
			cout << "WooHoo!\n";
		}
		else if ( passi == percorsoWin)
		    quanti++;
		trovato = true;
	}
	else 
	{
		for(int i =0; i < vettoreGrafo[nodo].size(); i++)
		{
			if(nodiVisti[vettoreGrafo[nodo][i]] == false)
			{
				if(SoiRiva(dest, vettoreGrafo[nodo][i], passi+1))
				{
				    trovato = true;
					
				}
				
			}
		}
		
	}
	if(trovato)
		nodiVisti[nodo] = false;
	return trovato;
}

void FaiRoba(int NumNodi)
{
	int distanza = 0;
	for(int i = 0; i < NumNodi; i++)
	{
		for(int j = i+1; j < NumNodi; j++)
		{
			ResettaEsplorati(0,NumNodi);
			percorsoWin = 100000;
			quanti = 0;
			if(SoiRiva(j,i,0))
			{
				cout << percorsoWin << endl;
				distanza = max(distanza, percorsoWin);
			}
		}
		
	}
	quanti = distanza;
	// cout << distanza;
}

int main()
{
	int NumNodi;
	int NumArchi;
	
	ifstream in("input.txt");
	in >> NumNodi >> NumArchi;
    
	percorsoWin = NumArchi;	   
	nodiVisti = new bool[NumNodi];
  	
	vettoreGrafo = new vector<int>[NumNodi];
	
	int orig, dest;
	
	for(int i=0;i<NumArchi;i++)
	{    
		in >> orig >> dest;
		vettoreGrafo[orig].push_back(dest);
	}
	
	in.close();
	
	StampaGrafo(NumNodi);
	
	FaiRoba(NumNodi);
	ofstream oo;
	oo.open("output.txt");
	oo << quanti;
	
	oo.close();
    
	return 0;
}
