#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

int ** matrice;

void PrintaMatrice(int nRighe, int nColonne)
{
  for(int i=0;i<nRighe;i++)
  {    
    for(int j = 0; j < nColonne; j++)
	{
	  cout << matrice[i][j] << " ";
	}
	cout << endl;
  }
}

int kadane2D(int nRighe, int nColonne)
{  

    int *columnSum = new int[nColonne];
    int s=0,S=0,t;
    for(int row = 0; row<nRighe; row++)
    {
        for(int i=0; i<nColonne; i++) columnSum[i] = 0;
        for(int x = row; x<nRighe; x++)
        {
            s = 0;
            t = 0;
            for(int i=0; i<nColonne; i++)
            {
                columnSum[i]+=matrice[x][i];
                t+=columnSum[i];
                if(t>s)
                    s = t;
                if(t<0)
                    t = 0;
            }
            if(s>S)
                S = s;
        }
    }
    delete [] columnSum;
    return S;
}

int main()
{
  int nRighe;
  int nColonne;
  
  ifstream in("input.txt");
  in >> nRighe >> nColonne;
  
  matrice = new int*[nRighe];
  
  for(int i=0;i<nRighe;i++)
  {    
    matrice[i] = new int[nColonne];
    for(int j = 0; j < nColonne; j++)
	{
	  in >> matrice[i][j];
	}
  }
  
  //PrintaMatrice(nRighe, nColonne);
  
  in.close();/*
  int maxSottMat = matrice[0][0];
  for(int rigaIni = 0; rigaIni < nRighe; rigaIni++)
  {
	for(int colonnaIni = 0; colonnaIni < nColonne; colonnaIni++)
	{ 
		for(int rigaFine = rigaIni; rigaFine < nRighe; rigaFine++)
		{	
			
			for(int colonnaFine = colonnaIni; colonnaFine < nColonne; colonnaFine++)  	 
			{   
			    maxSottMat = max(maxSottMat, sommaMatrice(rigaIni,colonnaIni,rigaFine+1,colonnaFine+1));
			}
		}
	}
  } */
  
  
  int somma = kadane2D(nRighe, nColonne);
	 cout << somma << endl;
	 
  ofstream oo;
  oo.open("output.txt");
  oo << somma;
  
  oo.close();
    
  return 0;
}
