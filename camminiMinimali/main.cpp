#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

bool * nodiVisti;
int * percorsi;
int quanti;
int percorsoWin;

vector<int> * vettoreGrafo;
int * daQuiCiMetto;

void StampaGrafo(int NumNodi)
{
  for(int i = 0; i < NumNodi; i++)
  {
	cout << "Ha " << vettoreGrafo[i].size() << " figli\n";
	cout << "Nodo " << i << " -> ";
	for(int j = 0; j < vettoreGrafo[i].size(); j++)
		cout << vettoreGrafo[i][j] << " ";
	cout << endl;
  } 
}

void ResettaEsplorati(int start, int end)
{
  for(int i = start; i < end; i++)
	nodiVisti[i] = false;
}

bool SoiRiva(int dest, int nodo, int passi)
{
	nodiVisti[nodo] = true;
	bool trovato = false;
	
	if(nodo == dest)
	{
		if(passi < percorsoWin)
		{
		    percorsoWin = passi;
			daQuiCiMetto[nodo] = 0;
			quanti = 1;
		}
		else if ( passi == percorsoWin)
		    quanti++;
		trovato = true;
	}
	else if(daQuiCiMetto[nodo] < 100000) 
	{
	  //cout << "Nodo " << nodo << " ci metto in tutto: " << passi+daQuiCiMetto[nodo] << " ed ero a " << passi << endl;
	  if(passi+daQuiCiMetto[nodo] < percorsoWin)
		{
		    percorsoWin = passi+daQuiCiMetto[nodo];
			//daQuiCiMetto[nodo] = 0;
			quanti = 1;
		}
		else if ( passi+daQuiCiMetto[nodo] == percorsoWin)
		    quanti++;
		trovato = true;
	}
	else 
	{
		for(int i =0; i < vettoreGrafo[nodo].size(); i++)
		{
			if(nodiVisti[vettoreGrafo[nodo][i]] == false)
			{
				if(SoiRiva(dest, vettoreGrafo[nodo][i], passi+1))
				{
				    daQuiCiMetto[nodo] = min(daQuiCiMetto[nodo], daQuiCiMetto[vettoreGrafo[nodo][i]]+1);
					trovato = true;
				}
				
			}
		}
		
	}
	if(trovato)
		nodiVisti[nodo] = false;
	return trovato;
}

int main()
{
  int NumNodi;
  int NumArchi;
  int NodoStart;
  int NodoFinish;

  quanti = 0;

  ifstream in("input.txt");
  in >> NumNodi >> NumArchi >> NodoStart >> NodoFinish;
    
  percorsoWin = NumArchi;	   
  nodiVisti = new bool[NumNodi];
  
  ResettaEsplorati(0,NumNodi);
	
  vettoreGrafo = new vector<int>[NumNodi];
  
  daQuiCiMetto = new int[NumNodi] ;
  for(int i =0; i < NumNodi; i++)
    daQuiCiMetto[i] = 100000;
  
  int orig, dest;
		     
  for(int i=0;i<NumArchi;i++)
  {    
    in >> orig >> dest;
    vettoreGrafo[orig].push_back(dest);
	//cout << orig << " " << dest << endl;
  }
  
  
  
  in.close();
  //StampaGrafo(NumNodi);
  
  SoiRiva(NodoFinish,NodoStart,0);
  //int min = QualeMinore(NumNodi);

  for(int i =0; i < NumNodi; i++)
    cout << i << " tempo: " <<daQuiCiMetto[i] << endl;   
	 
  //StampaPercorsi(NumArchi);
    //cout << quanti << endl;
  cout << percorsoWin << " " << quanti; 
  ofstream oo;
  oo.open("output.txt");
  oo << percorsoWin << " " << quanti;
  
  oo.close();
    
  return 0;
}
