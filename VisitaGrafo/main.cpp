#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

bool * nodiVisti;
int quanti;

vector<int> * vettoreGrafo;

void Explora(int nodo)
{
 // cout << "Sono nel nodo " << nodo << endl;
  nodiVisti[nodo] = true;
for (int i = 0; i < vettoreGrafo[nodo].size(); i++)
  {
   // cout << "sono al sottonodo " << i << " di " << vettoreGrafo[nodo].size() << endl;
	if(nodiVisti[vettoreGrafo[nodo][i]] == false)
	  Explora(vettoreGrafo[nodo][i]);
  }
}

int main()
{
  int NumNodi;
  int NumArchi;
  int NodoStart;

  quanti = 0;

  ifstream in("input.txt");
  in >> NumNodi >> NumArchi >> NodoStart;
       
  nodiVisti = new bool[NumNodi];
  for(int i = 0; i < NumNodi; i++)
	nodiVisti[i] = false;
	
  vettoreGrafo = new vector<int>[NumNodi];
  
  int orig, dest;
		     
  for(int i=0;i<NumArchi;i++)
  {    
    in >> orig >> dest;
    vettoreGrafo[orig].push_back(dest);
	//cout << orig << " " << dest << endl;
  }
  
  
  
  in.close();
  
  Explora(NodoStart);
  /*
  for(int i = 0; i < NumNodi; i++)
  {
	cout << "Ha " << vettoreGrafo[i].size() << " figli\n";
	cout << "Nodo " << i << " -> ";
	for(int j = 0; j < vettoreGrafo[i].size(); j++)
		cout << vettoreGrafo[i][j] << " ";
	cout << endl;
  } 
  */
  
  for(int i = 0; i < NumNodi; i++)
  {
    if(nodiVisti[i])
		quanti++;
  }
  
  //cout << quanti << endl;
	 
  ofstream oo;
  oo.open("output.txt");
  oo << quanti;
  
  oo.close();
    
  return 0;
}
